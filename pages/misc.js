import Layout from '../components/layout';
import Image from 'next/image';
import poster from '../public/clothes.png';
import logo from '../public/logo.png';
export default function Misc() {
  return <Layout>
    <div className="py-10 px-5 bg-white rounded-md shadow-lg w-3/4 md:max-w-2xl">
      <Image src={poster} alt="Poster"/>
    </div>
    <div className="py-10 px-5 bg-white rounded-md shadow-lg w-3/4 md:max-w-2xl">
      <Image src={logo} alt="Logo"/>
    </div>
  </Layout>;
}
