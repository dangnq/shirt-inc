import Layout from '../components/layout';
import { serialize } from 'next-mdx-remote/serialize';
import { MDXRemote } from 'next-mdx-remote';
import { promises as fs } from 'fs';
import path from 'path';
import Image from 'next/image';
import imageSize from 'rehype-img-size';

const components = {
  img: (props) => (
    // height and width are part of the props, so they get automatically passed here with {...props}
    <Image {...props} layout="responsive" alt=""/>
  ),
};
export default function About({ source }) {
  return <Layout>
    <article className="prose py-10 px-5 bg-white rounded-md shadow-lg w-3/4 md:max-w-2xl">
      <MDXRemote {...source} components={components}/>
    </article>
  </Layout>;
}

export async function getStaticProps() {
  const postFile = path.join(process.cwd(), 'database', 'posts', 'about.md');
  const data = await fs.readFile(postFile, 'utf-8');
  const markdownSource = await serialize(data, {
    mdxOptions: {
      // use the image size plugin, you can also specify which folder to load images from
      // in my case images are in /public/images/, so I just prepend 'public'
      rehypePlugins: [[imageSize, { dir: 'public' }]],
    },
  }); ;
  return {
    props: {
      source: markdownSource,
    },
  };
}
