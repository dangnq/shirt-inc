import { useRouter } from 'next/router';
import Link from 'next/link';
import React, { useState } from 'react';

const NavLink = ({ href, name }) => {
  const { asPath } = useRouter(); const ariaCurrent = href === asPath ? 'page' : undefined;
  return (
    <Link href={href}>
      <a aria-current={ariaCurrent} className={'block py-2 pr-4 pl-3 ' + (ariaCurrent ? 'text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0' : 'text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0')}>{name}</a>
    </Link >
  );
};

export default function Layout() {
  const [collapse, setCollapse] = useState(true);
  function toggleCollapse() {
    setCollapse(!collapse);
  }

  const pageList = [
    { 'title': 'Home', 'link': '/' },
    { 'title': 'About', 'link': '/about' },
    { 'title': 'Misc.', 'link': '/misc' },
    { 'title': 'Blog', 'link': '/blog' },
    { 'title': 'Contact', 'link': '/contact' },

  ];
  return <nav className="bg-white opacity-95 backdrop-blur-3xl border-gray-200 px-2 sm:px-4 py-2.5 rounded shadow-lg w-3/4 md:max-w-2xl mx-auto sticky top-0 z-50">
    <div className="container flex flex-wrap justify-between items-center mx-auto">
      <span className="self-center text-xl font-semibold whitespace-nowrap">Shirt Inc.</span>
      <button onClick={toggleCollapse} className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200" aria-controls="mobile-menu-2" aria-expanded="false">
        <span className="sr-only">Open main menu</span>
        <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>
        <svg className="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
      </button>
      <div className={'md:block md:w-auto w-full ' + (collapse ? 'hidden' : '')}>
        <ul className="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
          {pageList.map((d, idx) => {
            return <li key={idx}>
              <NavLink href={d.link} name={d.title} />
            </li>;
          })}
        </ul>
      </div>
    </div>
  </nav>;
}
