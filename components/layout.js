import Navbar from '../components/navbar';
export default function Layout({ children }) {
  return <div className="h-screen min-h-full">
    <Navbar />
    <div className="grid place-items-center mx-auto">
      {children}
    </div>
  </div>;
}
