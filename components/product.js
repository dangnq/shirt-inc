import Typing from './typing';
import Image from 'next/image';
import React, { useState } from 'react';

// The function for each product
export default function Product({ d }) {
  const [sizeState, setSize] = useState('');
  function toggleSize(value) {
    if (value == sizeState) {
      setSize('');
    } else {
      setSize(value);
    }
  }
  const [likeState, setLike] = useState(false);
  function toggleLike() {
    setLike(!likeState);
  }
  return <section className="flex flex-col md:flex-row gap-11 py-10 px-5 bg-white rounded-md shadow-md hover:drop-shadow-xl w-3/4 md:max-w-2xl mt-4 mb-2">
    <div className="text-emerald-500 flex flex-col justify-between">
      <Image src={'/' + d.image} layout="responsive" alt="<> Hoodie" width={d.w} height={d.h} />
      <div>
        <small className="uppercase">choose size</small>
        <div className="flex flex-wrap md:flex-nowrap gap-1">
          {d.sizes.map((sizes, sizeId) => {
            if (sizes.availability) {
              return <button key={sizeId}
                className={'grid place-items-center border px-3 py-2 transition ' + (sizeState == sizes.size ? 'bg-emerald-500 text-white' : 'hover:bg-emerald-500 hover:text-white')}
                onClick={() => toggleSize(sizes.size)}>{sizes.size}</button>;
            } else {
              return <button className="grid place-items-center border px-3 py-2 cursor-not-allowed text-gray-300 transition" key={sizeId}>{sizes.size}</button>;
            }
          })}
        </div>
      </div>
    </div>


    <div className="text-emerald-500">
      <small className="uppercase">{d.type}</small>
      <h3 className="uppercase text-black text-2xl font-medium">
        <span>{d.name}</span>
        <span>&nbsp;</span>
        <Typing />
      </h3>
      <h3 className="text-2xl font-semibold mb-7">${d.price}</h3>
      <small className="text-black">{d.description}</small>
      <div className="flex gap-0.5 mt-4">
        <button className="bg-emerald-600 hover:bg-emerald-500 focus:outline-none transition text-white uppercase px-8 py-3">add to cart</button>
        <button className={'bg-emerald-600 hover:bg-emerald-500 focus:outline-none transition text-white uppercase p-3 ' + (likeState ? 'text-red-400' : 'text-white')} onClick={toggleLike}>
          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" className="bi bi-suit-heart-fill" viewBox="0 0 16 16">
            <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z" />
          </svg>
        </button>
      </div>
    </div>
  </section>;
}
